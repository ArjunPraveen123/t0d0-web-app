import React from 'react'
import TodoItem from './Todo_item'
import PropTypes from 'prop-types'

class Todos extends React.Component{

    
    render(){
        console.log(this.props.todos)
        return this.props.todos.map((todo) => (
            <TodoItem delete={this.props.delete} key={todo.id} todo={todo} markComplete = {this.props.markComplete}/>
        ))
    }
}

//goodpractice
Todos.propTypes = {
    todos: PropTypes.array.isRequired
}

export default Todos

