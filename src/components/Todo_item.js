import React, { Component } from 'react'
import PropTypes from 'prop-types'


import './todoitems.css'
import { findRenderedComponentWithType } from 'react-dom/test-utils'



class TodoItem extends Component {
    
    styling = () => {
        if(this.props.todo.completed){
            return {
                textDecoration: 'line-through'
            }
        }
    }

   
    //either use arrow functions or use function.bind since mark complete doesnt have access to class components
    
    render() {
        const {id,title} = this.props.todo
        return (
            
            <div className='title'>
            
                <div className="custom-underline" style={this.styling()}> 
                <input type='checkbox' onChange={this.props.markComplete.bind(this,id)}/>

                    {'                '}{this.props.todo.title} 
                    
                </div>
                <button onClick={this.props.delete.bind(this,id)} style={btnstyle}>x</button>
            </div>
        )
    }
}

const btnstyle = {
    background: '#ff0000',
    color: 'white',
    border: 'none',
    padding: '25px 25px',
    fontSize: '24px',
    float: 'right',
    cursor: 'pointer'
}
//goodpractice
TodoItem.propTypes = {
    todos: PropTypes.object.isRequired
}
export default TodoItem
