import React, { Component } from 'react'

export class Addtodo extends Component {
    state = {
        title : ''
    }

    change = (e) => {
        this.setState({[e.target.name] : e.target.value})
    }

    onSubmit = (e) => {
        e.preventDefault()
        this.props.addtodo(this.state.title)
        this.setState({title: ''})
    }

    render() {
        return (
            <form>
                <input type="text" name='title' placeholder='Add a task' 
                style={{flex : 10, padding: '5px'}}  value={this.state.title} onChange={this.change}/>
                <input type="submit" value='submit' className='btn' style={{flex: 1}} onClick={this.onSubmit}/>
            </form>
        )
    }
}

export default Addtodo
