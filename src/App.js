import React from 'react';
import Todos from './components/Todo'
import Addtodo from './components/Addtodo'
import { v4 as uuidv4 } from 'uuid';
import './App.css'


class App extends React.Component{

  state = {
    todos:[
      {  
        id : uuidv4(), 
        title: 'Follow the List',
        completed : false
      },
      {
        id : uuidv4(),
        title: 'Eat your meals on time',
        completed : false
      },
      {
        id : uuidv4(),
        title: 'Drink 8 litres of water',
        completed : false
      }
    ]
  }


  markComplete = (id) => {
    console.log(id)
    this.setState(
      {todos: this.state.todos.map(todo => {
        if(todo.id === id){
          todo.completed = !todo.completed
        }
        return todo
       })}
    )
  }
// use filter 
  delete = (id) => {
    this.setState({
      todos: [...this.state.todos.filter(todo => todo.id !== id)]
    })
  }
  
  addTodo = (title)=> {
    const newTodo = {
      id : uuidv4(),
      title: title,
      completed : false
    }
    this.setState({todos: [...this.state.todos, newTodo]})
  }

  render(){
    
    return (
      <div className="App">
        <Todos todos={this.state.todos} markComplete = {this.markComplete} delete={this.delete}/>
        
        <Addtodo addtodo = {this.addTodo}/>
      </div>
      
    );
  }
}

export default App;

